mosaik-csv
==========

This is pseudo simulator that presents CSV data sets to mosaik as models.


Installation
------------

::

    $ pip install mosaik-csv

You can run the tests with::

    $ git clone https://bitbucket.org/mosaik/mosaik-csv
    $ cd mosaik-csv
    $ pip install -r requirements.txt
    $ py.test
